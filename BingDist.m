% The Bing Dist.
% This class represents a d-dimensional Bingham distribution.
% This is extended implementation of BinghamDistribution from
% libDirectional
%
%   Notation:
%   In this class, d represents the dimension of the distribution.
%   Currently, there is no support for uniform Bingham distributions.
%
%
% see
% C. Bingham, "An antipodally symmetric distribution on the sphere",
% The Annals of Statistics, vol. 2, no. 6, pp. 1201-1225, Nov. 1974.

classdef BingDist < BinghamDistribution
        
    methods
        %%
        function B = BingDist(varargin)
            % Constructs a Bingham distribution object.
            % Parameters:
            %   Z_ (d x 1 vector)
            %       concentration parameters (have to be increasing)
            %   M_ (d x d matrix)
            %       orthogonal matrix that describes rotation
            % or
            %   A_ (d x d matrix)
            %       symmetric, positive semi-definite matrix 
            %       constructed as M_*diag(Z)*M_'
            % Returns:
            %   B (BinghamDistribution)
            %       an object representing the constructed distribution

            assert(0<nargin && nargin<=2, 'wrong number of inputs')
            if nargin == 1
                A_ = varargin{1};
                % Test for symmetricity with tolerance
                tol = 10e-12;
                sym = sum(sum(abs((A_)-(A_)')));
                assert(sym<tol, 'A_ is not symmetric')
                % In case of almost symmetric, make symmetric
                if sym>0
                    A_ = tril(A_,-1)'+tril(A_);
                    disp("Warning: argument was made symmetric")
                end
                [~, Z_, M_] = svd(A_);
                Z_ = -diag(Z_);
            end

            if nargin == 2
                Z_ = varargin{1};
                M_ = varargin{2};
            end

            % Check Type
            assert(isa(Z_,'double'), 'Z_ is not type double')
            assert(isa(M_,'double'), 'M_ is not type double')

            dim = size(M_,1);

            % Check Dimensions
            if (size(Z_,2) ~= 1)
                Z_ = Z_';
            end
            assert(size(Z_,2) == 1, 'Z_ is not vector')
            assert(size(M_,2) == dim, 'M is not square');
            assert(size(Z_,1) == dim, 'Z has wrong number of rows');
            assert(size(Z_,2) == 1, 'Z needs to be column vector');
            
            % Enforce last entry of Z to be zero
            % Z is addition constant invariant
            if (Z_(dim, 1) ~= 0)
                Z_ = Z_ - Z_(end);
            end
            
            % Enforce z1<=z2<=...<=z(d-1)<=0=z(d)
            assert(all(Z_(1:end-1) <= Z_(2:end)), 'values in Z have to be ascending');
            
            %enforce that M is orthogonal
            epsilon = 0.001;
            assert (max(max(M_*M_' - eye(dim,dim))) < epsilon, 'M is not orthogonal');

            B@BinghamDistribution(Z_, M_); % Call BinghamDistribution constructor
        end
        %%
        function h = plot(this, faces, gridFaces, colmap)
            % Plots the pdf of a hyperspherical distribution
            %
            % Parameters:
            %   faces (scalar)
            %       Number of faces for 3D Plot (default 100).
            %   gridFaces (scalar)
            %       Number of grid faces for 3D Plot (default 20, 0 to disable).
            %   colmap (n x 3 matrix)
            %       Colormap for 3D figure (default summer). 
            %       To obtain fake contour plot, use colmap with stripes.
            %       E.g.: repmat([ones(50,3)*0.9; zeros(50,3)], 5, 1)
            arguments
                this (1,1) AbstractHypersphericalDistribution
                faces (1,1) double {mustBeNonnegative,mustBeInteger} = 100
                gridFaces (1,1) double {mustBeNonnegative,mustBeInteger} = 20
                colmap (:,3) double = summer;
            end
            
            colormap(colmap)
            holdStatus=ishold;
            hold on
            switch this.dim
                case 2
                    phi = linspace(0,2*pi,360);
                    x = [cos(phi); sin(phi)];
                    p = this.pdf(x);
                    h = this.plot3color(x(1,:),x(2,:),p,'EdgeColor','flat');
                    M = this.M*1.1;
                    plot([0,M(1,1)], [0,M(2,1)], 'k', LineWidth=2)
                    plot([0,M(1,2)], [0,M(2,2)], 'r', LineWidth=2)
                    zlabel('p')
                case 3
                    h = plot@BinghamDistribution(this, faces, gridFaces);
                    h(end).FaceAlpha = 0.7;
                    M = this.M*1.1;
                    plot3([0,M(1,1)], [0,M(2,1)], [0,M(3,1)], 'k', LineWidth=2)
                    plot3([0,M(1,2)], [0,M(2,2)], [0,M(3,2)], 'k', LineWidth=2)
                    plot3([0,M(1,3)], [0,M(2,3)], [0,M(3,3)], 'r', LineWidth=2)
                    zlabel('z_3')
%                     zlim([-1.1 1.1])
                otherwise
                    error('Cannot plot Bingham distribution with this number of dimensions');
            end
            if ~holdStatus
                hold off
            end
            axis square
            grid on
            xlabel('z_1')
            ylabel('z_2')
%             xlim([-1.1 1.1])
%             ylim([-1.1 1.1])
        end
        %%
        function h = plotGnomic(this, faces, options)
            % Plots the gnomic transformation of a Bingham distribution pdf
            %
            % Parameters:
            %   faces (scalar)
            %       Number of faces for 3D Plot (default 100).
            %   colmap (n x 3 matrix)
            %       Colormap for 3D figure (default summer). 
            %       To obtain fake contour plot, use colmap with stripes.
            %       E.g.: repmat([ones(50,3)*0.9; zeros(50,3)], 5, 1)
            arguments
                this (1,1) AbstractHypersphericalDistribution
                faces (1,1) double {mustBeNonnegative,mustBeInteger} = 100;
                options.plotStyle (1,1) string = "2d";
                options.colmap (:,3) double = summer;
                options.slice (:,1) double {mustBeNonnegative,mustBeInteger} = 1:this.dim-1;
            end
            assert(all(ismember(options.slice, 1:this.dim-1)),...
                "Cannot slice distribution with inputted slice")
            assert(length(options.slice)<=2 && length(options.slice)>=1,...
                'Cannot plot Bingham distribution with this number of slices/dimensions')

            holdStatus=ishold;
            hold on
            axis square
            h = [];
            if length(options.slice) == 1
                [Xgnom, ~, c] = this.dimensionSlice(options.slice, faces);
                h = [h plot(Xgnom, c, 'r')];
                xlim([min(Xgnom) max(Xgnom)]);
                xlabel("x_" + num2str(options.slice))
            % If offdiagonal
            elseif length(options.slice) == 2
                % Generate slices
                [Ygnom, Xgnom, c] = this.dimensionSlice(options.slice, faces);
                if options.slice(1)>options.slice(2)
                    tmp = Xgnom;
                    Xgnom = Ygnom;
                    Ygnom = tmp;
                end
                % Plot slices
                switch options.plotStyle
                    case "2d"
                        ML = this.mlGnom;
                        [~,hcont] = contourf(Xgnom,Ygnom,c);
                        h = [h hcont plot([ML(options.slice(2)) ML(options.slice(2))], [min(min(Ygnom)) max(max(Ygnom))], 'r')];
                        h = [h plot([min(min(Xgnom)) max(max(Xgnom))], [ML(options.slice(1)) ML(options.slice(1))], 'r')];
                    case "3d"
                        h = [h surf(Xgnom, Ygnom, c, 'FaceAlpha', 0.8, 'Edgecolor', 'none')];
                        h = [h plot3(Xgnom(ceil(faces/2)+1,:), ...
                            Ygnom(ceil(faces/2)+1,:), ...
                            c(ceil(faces/2)+1,:), ...
                            'r', 'LineWidth', 2)];
                        h = [h plot3(Xgnom(:,ceil(faces/2)+1), ...
                            Ygnom(:,ceil(faces/2)+1), ...
                            c(:,ceil(faces/2)+1), ...
                            'r', 'LineWidth', 2)];
                        grid on
                        view(-30, 80)
                    otherwise
                        error('PlotStyle can be only "2d" or "3d"');
                end
                clim([0 this.pdf(this.ml)])
                xlabel("x_" + num2str(options.slice(2)))
                ylabel("x_" + num2str(options.slice(1)))
            end
            if ~holdStatus
                hold off
            end
        end
        %%
        function h = displayGnomicTransformation(this, faces, colmap)
            % Displays the gnomic transformation of a Bingham distribution pdf
            %
            % Parameters:
            %   faces (scalar)
            %       Number of faces for 3D Plot (default 100).
            %   colmap (n x 3 matrix)
            %       Colormap for 3D figure (default summer). 
            %       To obtain fake contour plot, use colmap with stripes.
            %       E.g.: repmat([ones(50,3)*0.9; zeros(50,3)], 5, 1)
            arguments
                this (1,1) AbstractHypersphericalDistribution
                faces (1,1) double {mustBeNonnegative,mustBeInteger} = 100
                colmap (:,3) double = summer;
            end
            
            colormap(colmap)

            ML = this.M(:,end);
            ML = -ML(1:end-1)/ML(end);

            if this.dim == 3
                offset = this.offsetFromZ(1); % empirical equation
                phi1 = linspace(ML(1)-offset,ML(1)+offset,faces);
                offset = this.offsetFromZ(2); % empirical equation
                phi2 = linspace(ML(2)-offset,ML(2)+offset,faces);

                [Xgnom,Ygnom] = meshgrid(phi1,phi2);
                Zgnom = -ones(faces);
                
                % Normalize vectors
                block(:,:,1) = Xgnom;
                block(:,:,2) = Ygnom;
                block(:,:,3) = Zgnom;
                block = block./vecnorm(block, 2, 3);

                X = block(:,:,1);
                Y = block(:,:,2);
                Z = block(:,:,3);

                % Evaluate p.d.f.
                c=reshape(this.pdf([X(:)';Y(:)';Z(:)']),size(Xgnom));

                % Plot Bingham
                hold on;
                this.plot(faces, 0)

                % Plot connecting lines
                for i = 1:10:faces
                    for y = 1:10:faces
                        plot3([Xgnom(i,y) X(i,y)], [Ygnom(i,y) Y(i,y)], [Zgnom(i,y) Z(i,y)], 'k')
                    end
                end

                % Plot contours
                [~,h] = contourf(Xgnom,Ygnom,c);
                h.ContourZLevel = -1;
                axis equal
            else
                error('Cannot plot gnomic transformation with this number of dimensions');
            end
        end
        %%
        function h = stairPlotGnomic(this, faces, options)
            % Plots the stairplot of Bingham pdf
            %
            % Parameters:
            %   faces (scalar)
            %       Number of faces for 3D Plot (default 100).
            %   options (Name value pairs)
            %       plotStyle
            %           Style plots 2d/3d (default 2d)
            %       colmap
            %           Colormap for 3D figure (default summer).
            %       triangular
            %           Select upper/lower triangular stairplot (default
            %           lower)
            arguments
                this (1,1) AbstractHypersphericalDistribution
                faces (1,1) double {mustBeNonnegative,mustBeInteger} = 100;
                options.plotStyle (1,1) string = "2d";
                options.colmap (:,3) double = summer;
                options.triangular (1,1) string = "lower"
            end
            h = [];
            if this.dim == 2
                h = [h this.plotGnomic(faces, options.colmap)];
            elseif this.dim > 2
                for col = 1:(this.dim-1)
                    for row = 1:(this.dim-1)
                        % If diagonal
                        if col == row
                            subplot(this.dim-1, this.dim-1, (row-1)*(this.dim-1) + row)
                            hold on
                            axis square
                            [Xgnom, ~, c] = this.dimensionSlice(row, faces);
                            h = [h plot(Xgnom, c, 'r')];
                            xlim([min(Xgnom) max(Xgnom)]);
                            xlabel("x_" + num2str(col))
                        % If offdiagonal
                        elseif col < row
                            % Generate slices
                            switch options.triangular
                                case "upper"
                                    subplot(this.dim-1, this.dim-1, (col-1)*(this.dim-1) + row)
%                                     [Ygnom, Xgnom, c] = this.dimensionSlice([row, col], faces);
                                    h = [h this.plotGnomic(faces,...
                                        "colmap", options.colmap,...
                                        "plotStyle", options.plotStyle,...
                                        "slice", [col row])];
                                case "lower"
                                    subplot(this.dim-1, this.dim-1, (row-1)*(this.dim-1) + col)
                                    h = [h this.plotGnomic(faces,...
                                        "colmap", options.colmap,...
                                        "plotStyle", options.plotStyle,...
                                        "slice", [row col])];
                                otherwise
                                    error('Triangular can be only "upper" or "lower"');
                            end
                        end
                    end
                end
            end
            colormap(options.colmap)
            clim([0 this.pdf(this.ml)])
            cbar = colorbar;
            set(cbar,'location','eastoutside',...
                'position',[0.93 0.168 0.022 0.7] );
        end
        %%
        function ML = ml(this, index)
            % Calculates maximum likelihood solution
            % One of antipodally symmetric solution is selected
            arguments
                this (1,1) AbstractHypersphericalDistribution
                index (:,1) double {mustBeNonnegative,mustBeInteger} = 1:length(this.M)
            end
            ML = this.M(:,end);
            ML = ML(index);
        end
        %%
        function ML = mlGnom(this, index)
            % Calculates maximum likelihood solution in gnomic
            % transformation
            arguments
                this (1,1) AbstractHypersphericalDistribution
                index (:,1) double {mustBeNonnegative,mustBeInteger} = 1:length(this.M)-1
            end
            ML = -this.M(1:end-1,end)/this.M(end,end);
            ML = ML(index);
        end
        %%
        function [Xgnom, Ygnom, p] = dimensionSlice(this, d, faces)
            % Returns values for dimensionSlice along dimensions d
            %
            % Parameters:
            %   d     (vector)
            %       vector of dimensions to slice (max 2)
            %   faces (scalar)
            %       Number of faces for plot (default 100).
            arguments
                this (1,1) AbstractHypersphericalDistribution
                d    (1,:) double
                faces (1,1) double {mustBeNonnegative,mustBeInteger} = 100
            end

            ML = this.mlGnom;
            switch length(d)
                case 1
                    offset1 = this.offsetFromZ(d);
                    Xgnom = sort([linspace(ML(d)-offset1,ML(d)+offset1, ...
                        faces) ML(d)]);
                    Ygnom = NaN;
        
                    % create block of gnomic coordinates
                    block = NaN(this.dim, faces+1);
                    for layer=1:this.dim-1
                        if layer == d
                            block(layer,:) = Xgnom;
                        else
                            block(layer,:) = ML(layer)*ones(1,faces+1);
                        end
                    end
                    block(this.dim,:) = -ones(1,faces+1);
                    block = block./vecnorm(block);
                    % find pdf
                    p = this.pdf(block);

                case 2
                    d = sort(d);
                    offset1 = this.offsetFromZ(d(1));
                    coord1 = sort([linspace(ML(d(1))-offset1,ML(d(1))+offset1,faces), ...
                        ML(d(1))]);
                    offset2 = this.offsetFromZ(d(2));
                    coord2 = sort([linspace(ML(d(2))-offset2,ML(d(2))+offset2,faces), ...
                        ML(d(2))]);
                    [Xgnom,Ygnom] = meshgrid(coord1,coord2);
        
                    % create block of gnomic coordinates
                    block = NaN(faces+1, faces+1, this.dim);
                    for layer=1:this.dim-1
                        if layer == d(1)
                            block(:,:,layer) = Xgnom;
                        elseif layer == d(2)
                            block(:,:,layer) = Ygnom;
                        else
                            block(:,:,layer) = ML(layer)*ones(faces+1);
                        end
                    end
                    block(:,:,this.dim) = -ones(faces+1);
                    % normalize coordinates
                    block = block./vecnorm(block, 2, 3);
                    % find pdf
                    p = reshape(this.pdf(reshape(block, [], this.dim)'), size(Xgnom));

                otherwise
                    error('Cannot slice Bingham distribution with this number of slice dimensions');
            end
        end
        %%
        function offset = offsetFromZ(this, index)
            % Gives offset that allows for visible display of pdf
            % Equation is empirical and based on nothing
            offset = -100/(this.Z(index)-10);
        end
    end
    
    methods (Static)
        %%
        function h = plot3color(X,Y,Z,varargin)
            arguments
                X (:,1) double
                Y (:,1) double
                Z (:,1) double
            end
            arguments (Repeating)
                varargin
            end
            h = surf([X, X], [Y, Y], [Z, Z.*0], [Z, Z], varargin{:});
        end
    end
end